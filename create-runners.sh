#!/bin/bash

set -o allexport; source .env; set +o allexport

docker volume create gitlab-runner01-config
docker volume create gitlab-runner02-config
docker volume create gitlab-runner03-config
docker volume create gitlab-runner04-config
docker volume create gitlab-runner05-config
docker volume create gitlab-runner06-config

docker run -d --name gitlab-runner01 --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-runner01-config:/etc/gitlab-runner \
    gitlab/gitlab-runner:latest

docker run -d --name gitlab-runner02 --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-runner02-config:/etc/gitlab-runner \
    gitlab/gitlab-runner:latest

docker run -d --name gitlab-runner03 --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-runner03-config:/etc/gitlab-runner \
    gitlab/gitlab-runner:latest

docker run -d --name gitlab-runner04 --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-runner04-config:/etc/gitlab-runner \
    gitlab/gitlab-runner:latest

docker run -d --name gitlab-runner05 --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-runner05-config:/etc/gitlab-runner \
    gitlab/gitlab-runner:latest

docker run -d --name gitlab-runner06 --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-runner06-config:/etc/gitlab-runner \
    gitlab/gitlab-runner:latest

