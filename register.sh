#!/bin/bash

set -o allexport; source .env; set +o allexport

docker run --rm -it -v gitlab-runner01-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register \
  --non-interactive \
  --url "${GITLAB_URL}" \
  --registration-token "${CICD_RUNNER_TOKEN}" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "🦊🍊 gitpod 1️⃣" \
  --tag-list "gitpod_01" \
  --run-untagged="true" \
  --locked="false"

docker run --rm -it -v gitlab-runner02-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register \
  --non-interactive \
  --url "${GITLAB_URL}" \
  --registration-token "${CICD_RUNNER_TOKEN}" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "🦊🍊 gitpod 2️⃣" \
  --tag-list "gitpod_02" \
  --run-untagged="true" \
  --locked="false"

docker run --rm -it -v gitlab-runner03-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register \
  --non-interactive \
  --url "${GITLAB_URL}" \
  --registration-token "${CICD_RUNNER_TOKEN}" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "🦊🍊 gitpod 3️⃣" \
  --tag-list "gitpod_03" \
  --run-untagged="true" \
  --locked="false"

docker run --rm -it -v gitlab-runner04-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register \
  --non-interactive \
  --url "${GITLAB_URL}" \
  --registration-token "${CICD_RUNNER_TOKEN}" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "🦊🍊 gitpod 4️⃣" \
  --tag-list "gitpod_04" \
  --run-untagged="true" \
  --locked="false"

docker run --rm -it -v gitlab-runner05-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register \
  --non-interactive \
  --url "${GITLAB_URL}" \
  --registration-token "${CICD_RUNNER_TOKEN}" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "🦊🍊 gitpod 5️⃣" \
  --tag-list "gitpod_05" \
  --locked="false"

docker run --rm -it -v gitlab-runner06-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register \
  --non-interactive \
  --url "${GITLAB_URL}" \
  --registration-token "${CICD_RUNNER_TOKEN}" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "🦊🍊 gitpod 6️⃣" \
  --tag-list "gitpod_06" \
  --run-untagged="true" \
  --locked="false"
  
