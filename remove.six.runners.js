const axios = require("axios")
require('dotenv').config({ path: './.env' });

const gitlabAPI = process.env.GITLAB_API
const groupId = process.env.GROUP_ID

let headers = {
  "Content-Type": "application/json",
  "Private-Token": process.env.GITLAB_TOKEN_ADMIN
}

let query = ({method, path, headers, data}) => {
  return axios({
    method: method,
    url: `${gitlabAPI}/${path}`,
    headers: headers,
    data: data !== null ? JSON.stringify(data) : null
  })
}

let runnersList = ({groupId, headers}) => query({
  method: "GET", 
  path: `groups/${groupId}/runners`,
  headers: headers
})

let removeRunner = ({runnerId, headers}) => query({
  method: "DELETE", 
  path: `runners/${runnerId}`,
  headers: headers
})

runnersList({
  groupId: groupId,
  headers: headers
})
.then(response => response.data)
.then(runners => {
  console.log("👋 runners:", runners)
  // Remove all runners (of the group) with a specific description
  runners.filter(runner => runner.description=="gitpod_01").forEach(
    runner => removeRunner({runnerId:runner.id, headers: headers})
                .then(response => console.log(response.status))
                .catch(error => console.log("😡", error))
  )
  runners.filter(runner => runner.description=="gitpod_02").forEach(
    runner => removeRunner({runnerId:runner.id, headers: headers})
                .then(response => console.log(response.status))
                .catch(error => console.log("😡", error))
  )
  runners.filter(runner => runner.description=="gitpod_03").forEach(
    runner => removeRunner({runnerId:runner.id, headers: headers})
                .then(response => console.log(response.status))
                .catch(error => console.log("😡", error))
  )
  runners.filter(runner => runner.description=="gitpod_04").forEach(
    runner => removeRunner({runnerId:runner.id, headers: headers})
                .then(response => console.log(response.status))
                .catch(error => console.log("😡", error))
  )
  runners.filter(runner => runner.description=="gitpod_05").forEach(
    runner => removeRunner({runnerId:runner.id, headers: headers})
                .then(response => console.log(response.status))
                .catch(error => console.log("😡", error))
  )
  runners.filter(runner => runner.description=="gitpod_06").forEach(
    runner => removeRunner({runnerId:runner.id, headers: headers})
                .then(response => console.log(response.status))
                .catch(error => console.log("😡", error))
  )
})
