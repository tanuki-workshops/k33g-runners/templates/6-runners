# 6-runners

This project allows the creation of 6 GitLab runners (for a group) in a Gitpod workspace.

1. Set a `GITLAB_TOKEN_ADMIN` variable (in the admin panel of Gitpod)
2. Add the id of the group to the `.env` file + `CICD_RUNNER_TOKEN` (for this project) with the token provided by the group
3. Commit the changes
4. Open this project in Gitpod


> Next step: 
> - Store all environment variables with the CI/CD environment variables
> - Get the values from the Gitpod Workspace with the GitLab API
